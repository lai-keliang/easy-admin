package com.mars.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 源码字节-程序员Mars
 */
public class LoggerUtils {

    private static final Logger logger = LoggerFactory.getLogger(LoggerUtils.class);

    public static void info(String s) {
        logger.info(s);
    }

    public static void info(String s, Object o) {
        logger.info(s, o);
    }

    public static void info(String s, Object... objects) {
        logger.info(s, objects);
    }

    public static void info(String s, Throwable throwable) {
        logger.info(s, throwable);
    }

    public static void error(String s) {
        logger.error(s);
    }

    public static void error(String s, Object o) {
        logger.error(s, o);
    }

    public static void error(String s, Object... objects) {
        logger.error(s, objects);
    }

    public static void error(String s, Throwable throwable) {
        logger.error(s, throwable);
    }
}
