package com.mars.module.system.service;

import com.mars.common.request.sys.SysMenuAddRequest;
import com.mars.common.request.sys.SysMenuQueryRequest;
import com.mars.common.request.sys.SysMenuUpdateRequest;
import com.mars.common.response.sys.SysMenuResponse;
import com.mars.common.response.sys.SysMenuResponse;
import com.mars.common.response.PageInfo;
import com.mars.common.response.sys.SysParentMenuResponse;
import com.mars.module.system.entity.SysMenu;

import java.util.List;

/**
 * 系统菜单
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-08-23 09:45:22
 */
public interface ISysMenuService {
    /**
     * 构建树形菜单
     *
     * @param queryDto queryDto
     * @return List<SysMenuVo>
     */
    List<SysMenuResponse> tree(SysMenuQueryRequest queryDto);

    /**
     * 获取菜单列表
     *
     * @param queryDto queryDto
     * @return PageVo<SysMenuVo>
     */
    PageInfo<SysMenuResponse> list(SysMenuQueryRequest queryDto);

    /**
     * 获取子菜单
     *
     * @param list list
     * @param id   id
     * @return List<SysMenuVo>
     */
    List<SysMenuResponse> getChildren(List<SysMenu> list, Long id);

    /**
     * 获取菜单详情
     *
     * @param id id
     * @return SysMenuVo
     */
    SysMenuResponse get(Long id);

    /**
     * 根据用户ID获取菜单
     *
     * @param userId userId
     * @return List<SysMenuVo>
     */
    List<SysMenuResponse> selectByUserId(Long userId);

    /**
     * 添加
     *
     * @param addDto addDto
     */
    void add(SysMenuAddRequest addDto);

    /**
     * 更新
     *
     * @param updateDto updateDto
     */
    void update(SysMenuUpdateRequest updateDto);

    /**
     * 删除
     *
     * @param id id
     */
    void delete(Long id);

    /**
     * 获取父级菜单列表
     *
     * @return List<SysParentMenuResponse>
     */
    List<SysParentMenuResponse> getParentMenu();
}
