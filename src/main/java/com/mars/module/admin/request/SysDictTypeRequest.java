package com.mars.module.admin.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.mars.common.request.PageRequest;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 字典类型请求对象 sys_dict_type
 *
 * @author mars
 * @date 2023-11-18
 */
@Data
@ApiModel(value = "字典类型对象")
@EqualsAndHashCode(callSuper = true)
public class SysDictTypeRequest extends PageRequest{



    /**
     * ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * 类型
     */
    @ApiModelProperty(value = "类型")
    private String type;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private Integer state;
}
