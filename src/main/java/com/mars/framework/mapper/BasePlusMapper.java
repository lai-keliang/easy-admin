package com.mars.framework.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 自定义Mapper
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-08-10 13:54:54
 */
public interface BasePlusMapper<T> extends MPJBaseMapper<T> {

    /**
     * 全字段更新，不会忽略null值
     *
     * @param entity 实体对象
     */
    void alwaysUpdateSomeColumnById(@Param("et") T entity);

    /**
     * 批量插入
     *
     * @param entityList 实体集合
     */
    void insertBatchSomeColumn(List<T> entityList);


}
